package com.example.wfa.myapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class Reset extends AppCompatActivity {
    private EditText editEmail;
    private Button BtnSend;
    private FirebaseAuth mAuth;
    private ProgressDialog PD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);
        setTitle("Reset sandi via Email");
        editEmail = (EditText) findViewById(R.id.edEmail);
        BtnSend = (Button) findViewById(R.id.btn_reset);

        PD = new ProgressDialog(this);
        PD.setMessage("Menghubungkan ke server...");
        PD.setCancelable(true);
        PD.setCanceledOnTouchOutside(false);
        PD = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();

        BtnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = editEmail.getText().toString().trim();
                if(email.isEmpty()){
                    editEmail.setError("email diperlukan");
                    editEmail.requestFocus();
                    return;
                }
                if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    editEmail.setError("Masukkan format email yang benar");
                    editEmail.requestFocus();
                    return;
                }
                PD.setMessage("Menghubungkan ke server...");
                PD.show();
                mAuth.sendPasswordResetEmail(email)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(Reset.this, "Link reset sandi telah dikirim ke email anda", Toast.LENGTH_LONG).show();
                                    finish();
                                    startActivity(new Intent(Reset.this, Signin.class));
                                } else {
                                    Toast.makeText(Reset.this, "Email belum terdaftar di server kami", Toast.LENGTH_SHORT).show();
                                    PD.dismiss();
                                }
                            }
                        });
            }
        });

    }

}

