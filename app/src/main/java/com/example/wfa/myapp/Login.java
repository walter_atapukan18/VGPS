package com.example.wfa.myapp;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
public class Login extends AppCompatActivity {
    private EditText mLoginEmailField, mLoginPassfield;
    private Button mLoginBtn;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private ProgressDialog PD;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("LOGIN");

        PD = new ProgressDialog(this);
        PD.setMessage("Menghubungkan ke server...");
        PD.setCancelable(true);
        PD.setCanceledOnTouchOutside(false);

        mAuth = FirebaseAuth.getInstance();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Pengguna");

        mLoginEmailField = (EditText) findViewById(R.id.edit_email);
        mLoginPassfield = (EditText) findViewById(R.id.editSandi);
        PD = new ProgressDialog(this);

        mLoginBtn = (Button)findViewById(R.id.button2);
        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cekLogin();

            }
        });
    }
    private void cekLogin() {
        String email = mLoginEmailField.getText().toString().trim();
        String password = mLoginPassfield.getText().toString().trim();
        if(email.isEmpty()){
            mLoginEmailField.setError("email diperlukan");
            mLoginEmailField.requestFocus();
            return;
        }
        if(password.isEmpty()){
            mLoginPassfield.setError("password diperlukan");
            mLoginPassfield.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            mLoginEmailField.setError("Masukkan format email yang benar");
            mLoginEmailField.requestFocus();
            return;
        }
        if(password.length()<6){
            mLoginPassfield.setError("minimum sandi adalah 6 karakter");
            mLoginPassfield.requestFocus();
            return;
        }
        if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)){
            PD.setMessage("Menghubungkan ke server ....");
            PD.show();
            mAuth.signInWithEmailAndPassword(email,password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {

                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {

                    if(task.isSuccessful()){
                      // cekUserexist();
                        finish();
                        Toast.makeText(Login.this, "Login berhasil", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));

                         PD.dismiss();
                    }else{
                        if (task.getException() instanceof FirebaseAuthWeakPasswordException)
                        { Toast.makeText(Login.this, "Weak Password", Toast.LENGTH_SHORT).show(); }
                        Toast.makeText(Login.this, "Email belum terdaftar/password salah", Toast.LENGTH_SHORT).show();
                        PD.dismiss();
                    }
                }
            });

        }
    }
    private void cekUserexist() {
        final String user_id = mAuth.getCurrentUser().getUid();
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild(user_id)){

                    Toast.makeText(Login.this, "Login berhasil ...", Toast.LENGTH_LONG).show();

                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    finish();
                              }else{
                    Toast.makeText(Login.this,"Akun belum terdaftar, silahkan DAFTAR",Toast.LENGTH_LONG);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
