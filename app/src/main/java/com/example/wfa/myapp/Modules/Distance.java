package com.example.wfa.myapp.Modules;

/**
 * Created by WFA on 19/02/2018.
 */

public class Distance {
    public String text;
    public int value;

    public Distance(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
