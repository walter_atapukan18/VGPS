package com.example.wfa.myapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Daftar extends AppCompatActivity {
    private EditText mNameField,mEmailField,mPasswordField;
    private Button mRegisterBtn,mLogin;
    private FirebaseAuth mAuth;
    private ProgressDialog PD;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("DAFTAR USER");

        PD = new ProgressDialog(this);
        PD.setMessage("Menghubungkan ke server ...");
        PD.setCancelable(true);
        PD.setCanceledOnTouchOutside(false);

        mAuth = FirebaseAuth.getInstance();
        PD = new ProgressDialog(this);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Pengguna");
        if (mAuth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(Daftar.this, MainActivity.class));

        }
        mEmailField = findViewById(R.id.email_field);
        mNameField = findViewById(R.id.nameField);
        mPasswordField = findViewById(R.id.passwordField);
        mRegisterBtn = findViewById(R.id.registerBtn);
        mLogin =  findViewById(R.id.btn_login);

        mRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startRegister();
            }
        });
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(getApplicationContext(),Login.class));

            }
        });
    }

     private void startRegister() {
       final String name = mNameField.getText().toString();
         final  String email = mEmailField.getText().toString();
         final String password = mPasswordField.getText().toString();

         if(name.isEmpty()){
             mNameField.setError("email diperlukan");
             mNameField.requestFocus();
             return;
         }
         if(email.isEmpty()){
             mEmailField.setError("email diperlukan");
             mEmailField.requestFocus();
             return;
         }
         if(password.isEmpty()){
             mPasswordField.setError("password diperlukan");
             mPasswordField.requestFocus();
             return;
         }
         if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
             mEmailField.setError("Masukkan format email yang benar");
             mEmailField.requestFocus();
             return;
         }
         if(password.length()<6){
             mPasswordField.setError("minimum sandi adalah 6 karakter");
             mPasswordField.requestFocus();
             return;
         }
         try{
             if (password.length() > 0 && email.length() > 0) {
                 PD.setMessage("Menghubungkan ke server...");
                 PD.show();
                 mAuth.createUserWithEmailAndPassword(email,password)
                         .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                             @Override
                             public void onComplete(@NonNull Task<AuthResult> task) {
if(!task.isSuccessful()){
    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
        finish();
        Toast.makeText(Daftar.this, "Email telah terdaftar", Toast.LENGTH_LONG).show();
    }
}else{
        String user_id = mAuth.getCurrentUser().getUid();
        String user_email = mAuth.getCurrentUser().getEmail();
        DatabaseReference current_db = mDatabase.child(user_id);
        current_db.child("name").setValue(name);
        current_db.child("status").setValue("My Status");
        current_db.child("email").setValue(user_email);
        finish();
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
    }
    PD.dismiss();
                    }
                             });
                         } else {
                     Toast.makeText(Daftar.this,"Fill All Fields",Toast.LENGTH_LONG).show();
                 }
             } catch (Exception e) {e.printStackTrace();
             }
         }
     };

