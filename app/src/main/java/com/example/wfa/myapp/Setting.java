package com.example.wfa.myapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class Setting extends AppCompatActivity {
    TextView mEmail, mUsername, mStatus,mjenis,idubahsandi;
    Button bBatal,bDaftar;
    DatabaseReference ambil;
    Spinner editspiner1;
    private FirebaseAuth mAuth;
    private TextView txtStatus,txtUsername;
    private FirebaseAuth.AuthStateListener mAuthlistener;
    private DatabaseReference mDatabase;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setTitle("Pengaturan Akun");

        mAuth = FirebaseAuth.getInstance();
        mAuthlistener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(mAuth.getCurrentUser() == null){
                    finish();
                    Intent loginIntent = new Intent(Setting.this,Daftar.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginIntent);
                }
            }
        };

        FirebaseUser user = mAuth.getCurrentUser();
        mEmail = (TextView) findViewById(R.id.texemail);
        mEmail.setText(user.getEmail());


        mUsername = findViewById(R.id.editUsername);
        mStatus = findViewById(R.id.editStatus);
        mjenis = findViewById(R.id.edittipekendaraan);
        editspiner1 = findViewById(R.id.editspiner);
        idubahsandi = findViewById(R.id.idubahsandi);

        bDaftar = findViewById(R.id.idPerbaharui);
        bDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ambil = FirebaseDatabase.getInstance().getReference("Pengguna").child(mAuth.getUid());

                String name = mUsername.getText().toString().trim();
//                String status = mStatus.getText().toString().trim();
                String esp = editspiner1.getSelectedItem().toString().trim();
                String jenis = mjenis.getText().toString().trim();

                ambil.child("name").setValue(name).toString();
                ambil.child("jenis_kendaraan").setValue(jenis).toString();
                ambil.child("type").setValue(esp).toString();

                finish();
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                Toast.makeText(Setting.this, "Update data berhasil", Toast.LENGTH_LONG).show();
            }
        });
        idubahsandi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),UbahSandi.class));
                        }
        });
        ambil = FirebaseDatabase.getInstance().getReference("Pengguna").child(mAuth.getUid());
        ambil.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getKey();
                HashMap<String, Object> value = (HashMap<String, Object>) dataSnapshot.getValue();
                String nama = value.get("name").toString();
                String mod = value.get("status").toString();
                String emai = value.get("email").toString();
                String jenis = value.get("jenis_kendaraan").toString();
                String spin = value.get("type").toString();
//                mStatus.setText(mod);
                mUsername.setText(nama);
                mjenis.setText(jenis);

        //        mEmail.setText(emai);

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
}