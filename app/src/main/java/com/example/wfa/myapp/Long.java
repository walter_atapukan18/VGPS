package com.example.wfa.myapp;

/**
 * Created by WFA on 13/02/2018.
 */

public class Long {
    String lon,lat,name,status;
    public Long(){}

    public Long(String lon, String lat, String name, String status) {
        this.lon = lon;
        this.lat = lat;
        this.name = name;
        this.status = status;
    }

    public String getLon() {
        return lon;
    }

    public String getLat() {
        return lat;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
