package com.example.wfa.myapp.Modules;

import java.util.List;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.List;
import com.example.wfa.myapp.Modules.Route;

public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
