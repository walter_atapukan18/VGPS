package com.example.wfa.myapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class Pengaturan extends AppCompatActivity {
    TextView mEmail, mUsername, mStatus;
    Button bBatal,bDaftar;
    DatabaseReference ambil;
    private FirebaseAuth mAuth;
    private TextView txtStatus,txtUsername;
    private FirebaseAuth.AuthStateListener mAuthlistener;
    private DatabaseReference mDatabase;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();

                if(mAuth.getCurrentUser() == null){
                    finish();
                    Intent loginIntent = new Intent(Pengaturan.this,Daftar.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginIntent);
                }

        mEmail = (TextView)findViewById(R.id.editEmail);
        mUsername = (TextView)findViewById(R.id.editUsername);
        mStatus = (TextView)findViewById(R.id.editStatus);

        bBatal = (Button) findViewById(R.id.idBatal);
        bDaftar = (Button) findViewById(R.id.idPerbaharui);
        bBatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent kehome = new Intent(Pengaturan.this, MainActivity.class);
                startActivity(kehome);

            }
        });
        bDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ambil = FirebaseDatabase.getInstance().getReference("Pengguna").child(mAuth.getUid());

                String name = mUsername.getText().toString().trim();
                String status = mStatus.getText().toString().trim();
                String em = mEmail.getText().toString().trim();

                ambil.child("name").setValue(name).toString();
                ambil.child("status").setValue(status).toString();
                ambil.child("email").setValue(em).toString();
                finish();
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                Toast.makeText(Pengaturan.this, "Update data berhasil", Toast.LENGTH_LONG).show();
            }
        });
        ambil = FirebaseDatabase.getInstance().getReference("Pengguna").child(mAuth.getUid());
        ambil.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getKey();
                HashMap<String, Object> value = (HashMap<String, Object>) dataSnapshot.getValue();
                String nama = value.get("name").toString();
                String mod = value.get("status").toString();
                String emai = value.get("email").toString();
                mStatus.setText(mod);
                mUsername.setText(nama);
                mEmail.setText(emai);
        }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        }
}
