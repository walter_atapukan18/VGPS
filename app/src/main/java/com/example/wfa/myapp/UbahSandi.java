package com.example.wfa.myapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class UbahSandi extends AppCompatActivity implements View.OnClickListener {
    private EditText EditSandi;
    private Button BtnUpdate;
    private ProgressDialog pesan;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_sandi);
        setTitle("Ubah sandi pengguna");
        auth = FirebaseAuth.getInstance();

        EditSandi = (findViewById(R.id.id_sandibaru));
        BtnUpdate = (findViewById(R.id.id_perbaharuisandi));
        pesan = new ProgressDialog(this);

        BtnUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v ==BtnUpdate){
            prosesubahsandi();
        }
    }

    private void prosesubahsandi() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String password = EditSandi.getText().toString().trim();
        if(password.isEmpty()){
            EditSandi.setError("anda belum memasukkan sandi");
            EditSandi.requestFocus();
            return;
        }
        if(user!=null){
            pesan.setMessage("Menghubungkan ke server ...");
            pesan.show();
            user.updatePassword(EditSandi.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                Toast.makeText(UbahSandi.this, "Sandi berhasil diperbaharui", Toast.LENGTH_LONG).show();
                                auth.signOut();
                                finish();
                                startActivity(new Intent(UbahSandi.this,Signin.class));
                                pesan.dismiss();
                            } else{
                                Toast.makeText(UbahSandi.this, "Sandi gagal diperbaharui", Toast.LENGTH_LONG).show();
                                pesan.dismiss();
                            }
                        }
                    });

        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
