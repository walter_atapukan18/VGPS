package com.example.wfa.myapp.Modules;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;
/**
 * Created by WFA on 19/02/2018.
 */
public class Route {
    public Distance distance;
    public Duration duration;
    public String endAddress;
    public LatLng endLocation;
    public String startAddress;
    public LatLng startLocation;

    public List<LatLng> points;
}
