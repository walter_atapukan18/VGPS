package com.example.wfa.myapp;

/**
 * Created by WFA on 11/02/2018.
 */

public class Lokasi {
 public String name, status;
 public double lat,lon;
 public Lokasi(){

 }

    public Lokasi(String name, String status, double lat, double lon) {
        this.name = name;
        this.status = status;
        this.lat = lat;
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
}
